package doubles.api.service;

import doubles.api.Constants;
import doubles.api.model.PersonDto;
import doubles.api.model.StatisticDto;
import doubles.api.repo.Repo;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@ApplicationScoped
public class StatisticService {

    @Inject private Repo repo;

    public StatisticDto calculate(String filter) {

        StatisticDto stats = null;

        if(filter.equals("enp"))
            stats = calcByENP();
        else if(filter.equals("snils"))
            stats = calcBySNILS();

        if(stats != null)
            stats.setCounts();

        return stats;
    }

    public String compare(long firstId, long secondId) {
        List<String> stats = new ArrayList<>();

        PersonDto first = repo.search(firstId);
        PersonDto second = repo.search(secondId);

        if(first == null || second == null)
            return "";

        if(!Objects.equals(first.getFirstName(), second.getFirstName()))
            stats.add(String.join("first | ", "first", first.getFirstName(), second.getFirstName()));
        if(!Objects.equals(first.getLastName(), second.getLastName()))
            stats.add(String.join(" | ", "last", first.getLastName(), second.getLastName()));
        if(!Objects.equals(first.getMiddleName(), second.getMiddleName()))
            stats.add(String.join(" | ", "middle", first.getMiddleName(), second.getMiddleName()));
        if(!Objects.equals(first.getSnils(), second.getSnils()))
            stats.add(String.join(" | ", "snils", first.getSnils(), second.getSnils()));
        if(!Objects.equals(first.getBirthDate(), second.getBirthDate()))
            stats.add(String.join(" | ", "birth", first.getBirthDate(), second.getBirthDate()));
        if(!Objects.equals(first.getEnp(), second.getEnp()))
            stats.add(String.join(" | ", "enp", first.getEnp(), second.getEnp()));

        return  String.join("\n", stats);
    }

    private StatisticDto calcBySNILS() {
        List<PersonDto> search = null;

        search = repo.searchBySNILS(200000);

        StatisticDto stats = new StatisticDto();

        stats.setDoublesBySNILS(search.size());

        Set<String> set = search.stream().map(PersonDto::getSnils).collect(Collectors.toSet());

        for(String snils : set) {

            List<PersonDto> tmp = search
                    .stream()
                    .filter(e -> e.getSnils().equals(snils))
                    .sorted(Comparator.comparing(PersonDto::getId))
                    .collect(Collectors.toList());


            if(!tmp.isEmpty() && tmp.size() > 1) {
                //#region Stage 1
                List<String> stage1Ids = tmp.stream()
                        .filter(
                                e ->
                                        Objects.equals(e.getEnp(), tmp.get(0).getEnp()) &&
                                                e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                                Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                                e.getFirstName().equals(tmp.get(0).getFirstName()) &&
                                                !e.getLastName().equals(tmp.get(0).getLastName())
                        ).map(e ->"" + e.getId()).collect(Collectors.toList());

                stage1Ids.add("" + tmp.get(0).getId());

                if(stage1Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_LAST)
                            .add(snils + "|" + String.join(",", stage1Ids));
                }
                //#endregion

                //#region Stage 2
                List<String> stage2Ids = tmp.stream()
                        .filter(
                                e -> Objects.equals(e.getEnp(), tmp.get(0).getEnp()) &&
                                        e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                        !Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                        e.getFirstName().equals(tmp.get(0).getFirstName()) &&
                                        e.getLastName().equals(tmp.get(0).getLastName())
                        ).map(e ->"" + e.getId()).collect(Collectors.toList());

                stage2Ids.add("" + tmp.get(0).getId());

                if(stage2Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_MIDDLE)
                            .add(snils + "|" + String.join(",", stage2Ids));
                }
                //#endregion

                //#region Stage 3
                List<String> stage3Ids = tmp.stream().parallel()
                        .filter(
                                e -> Objects.equals(e.getEnp(), tmp.get(0).getEnp()) &&
                                        e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                        (!Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                                !e.getLastName().equals(tmp.get(0).getLastName())) &&
                                        e.getFirstName().equals(tmp.get(0).getFirstName())
                        ).map(e ->"" + e.getId()).collect(Collectors.toList());

                stage3Ids.add("" + tmp.get(0).getId());

                if(stage3Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_MIDDLE_AND_LAST)
                            .add(snils + "|" + String.join(",", stage3Ids));
                }
                //#endregion

                //#region Stage 4
                List<String> stage4Ids = tmp.stream()
                        .filter(e -> !Objects.equals(e.getEnp(), tmp.get(0).getEnp()) &&
                                e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                e.getLastName().equals(tmp.get(0).getLastName()) &&
                                e.getFirstName().equals(tmp.get(0).getFirstName())
                        ).map(e -> "" + e.getId()).collect(Collectors.toList());

                stage4Ids.add("" + tmp.get(0).getId());

                if(stage4Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_ENP)
                            .add(snils + "|" + String.join(",", stage4Ids));
                }

                //#endregion

                //#region Stage 5
                List<String> stage5Ids = tmp.stream()
                        .filter(e -> !Objects.equals(e.getEnp(), tmp.get(0).getEnp()) &&
                                e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                !e.getLastName().equals(tmp.get(0).getLastName()) &&
                                e.getFirstName().equals(tmp.get(0).getFirstName())
                        ).map(e -> "" + e.getId()).collect(Collectors.toList());

                stage5Ids.add("" + tmp.get(0).getId());

                if(stage5Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_ENP_LAST)
                            .add(snils + "|" + String.join(",", stage5Ids));
                }

                //#endregion

                //#region Stage 6
                List<String> stage6Ids = tmp.stream()
                        .filter(e -> !Objects.equals(e.getEnp(), tmp.get(0).getEnp()) &&
                                e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                !Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                e.getLastName().equals(tmp.get(0).getLastName()) &&
                                e.getFirstName().equals(tmp.get(0).getFirstName())
                        ).map(e -> "" + e.getId()).collect(Collectors.toList());

                stage6Ids.add("" + tmp.get(0).getId());

                if(stage6Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_ENP_MIDDLE)
                            .add(snils + "|" + String.join(",", stage6Ids));
                }
                //#endregion

                //#region Stage 7
                List<String> stage7Ids = tmp.stream()
                        .filter(e -> !Objects.equals(e.getEnp(), tmp.get(0).getEnp()) &&
                                e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                !Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                !e.getLastName().equals(tmp.get(0).getLastName()) &&
                                e.getFirstName().equals(tmp.get(0).getFirstName())
                        ).map(e -> "" + e.getId()).collect(Collectors.toList());

                stage7Ids.add("" + tmp.get(0).getId());

                if(stage7Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_ENP_MIDDLE_LAST)
                            .add(snils + "|" + String.join(",", stage7Ids));
                }
                //#endregion
                //region stage 3 (full doubles)
                List<String> doubleIds = tmp.stream()
                        .filter(e -> Objects.equals(e.getSnils(), tmp.get(0).getSnils()) &&
                                e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                e.getFirstName().equals(tmp.get(0).getFirstName()) &&
                                e.getLastName().equals(tmp.get(0).getLastName()))
                        .map(e ->"" + e.getId()).collect(Collectors.toList());

                if(doubleIds.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_FULL).add(snils + "|" + String.join(",", doubleIds));
                }
                //endregion
            }
        }

        return stats;
    }

    private StatisticDto calcByENP() {
        List<PersonDto> search = null;

        search = repo.searchByENP(100000);

        StatisticDto stats = new StatisticDto();

        stats.setDoublesByENP(search.size());

        Set<String> set = search.stream().map(PersonDto::getEnp).collect(Collectors.toSet());

        for(String enp : set) {

            List<PersonDto> tmp = search
                    .stream()
                    .filter(e -> e.getEnp().equals(enp))
                    .sorted(Comparator.comparing(PersonDto::getId))
                    .collect(Collectors.toList());

            if(!tmp.isEmpty() && tmp.size() > 1) {
                //#region Stage 1
                List<String> stage1Ids = tmp.stream().parallel()
                        .filter(
                                e ->
                                        Objects.equals(e.getSnils(), tmp.get(0).getSnils()) &&
                                                e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                                Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                                e.getFirstName().equals(tmp.get(0).getFirstName()) &&
                                                !e.getLastName().equals(tmp.get(0).getLastName())
                        ).map(e ->"" + e.getId()).collect(Collectors.toList());

                stage1Ids.add("" + tmp.get(0).getId());

                if(stage1Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_LAST)
                            .add(enp + "|" + String.join(",", stage1Ids));
                }
                //#endregion

                //#region Stage 2
                List<String> stage2Ids = tmp.stream()
                        .filter(
                                e -> Objects.equals(e.getSnils(), tmp.get(0).getSnils()) &&
                                        e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                        !Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                        e.getFirstName().equals(tmp.get(0).getFirstName()) &&
                                        e.getLastName().equals(tmp.get(0).getLastName())
                        ).map(e ->"" + e.getId()).collect(Collectors.toList());

                stage2Ids.add("" + tmp.get(0).getId());

                if(stage2Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_MIDDLE)
                            .add(enp + "|" + String.join(",", stage2Ids));
                }
                //#endregion

                //#region Stage 3
                List<String> stage3Ids = tmp.stream()
                        .filter(
                                e -> Objects.equals(e.getSnils(), tmp.get(0).getSnils()) &&
                                        e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                        (!Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                                !e.getLastName().equals(tmp.get(0).getLastName())) &&
                                        e.getFirstName().equals(tmp.get(0).getFirstName())
                        ).map(e ->"" + e.getId()).collect(Collectors.toList());

                stage3Ids.add("" + tmp.get(0).getId());

                if(stage3Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_MIDDLE_AND_LAST)
                            .add(enp + "|" + String.join(",", stage3Ids));
                }
                //#endregion

                //#region Stage 4
                List<String> stage4Ids = tmp.stream()
                        .filter(e -> !Objects.equals(e.getSnils(), tmp.get(0).getSnils()) &&
                                e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                e.getLastName().equals(tmp.get(0).getLastName()) &&
                                e.getFirstName().equals(tmp.get(0).getFirstName())
                        ).map(e -> "" + e.getId()).collect(Collectors.toList());

                stage4Ids.add("" + tmp.get(0).getId());

                if(stage4Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_SNILS)
                            .add(enp + "|" + String.join(",", stage4Ids));
                }

                //#endregion

                //#region Stage 5
                List<String> stage5Ids = tmp.stream()
                        .filter(e -> !Objects.equals(e.getSnils(), tmp.get(0).getSnils()) &&
                                e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                !Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                e.getLastName().equals(tmp.get(0).getLastName()) &&
                                e.getFirstName().equals(tmp.get(0).getFirstName())
                        ).map(e -> "" + e.getId()).collect(Collectors.toList());

                stage5Ids.add("" + tmp.get(0).getId());

                if(stage5Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_SNILS_MIDDLE)
                            .add(enp + "|" + String.join(",", stage5Ids));
                }
                //#endregion

                //#region Stage 6
                List<String> stage6Ids = tmp.stream()
                        .filter(e -> !Objects.equals(e.getSnils(), tmp.get(0).getSnils()) &&
                                e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                !e.getLastName().equals(tmp.get(0).getLastName()) &&
                                e.getFirstName().equals(tmp.get(0).getFirstName())
                        ).map(e -> "" + e.getId()).collect(Collectors.toList());

                stage6Ids.add("" + tmp.get(0).getId());

                if(stage6Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_SNILS_LAST)
                            .add(enp + "|" + String.join(",", stage6Ids));
                }
                //#endregion

                //#region Stage 7
                List<String> stage7Ids = tmp.stream()
                        .filter(e -> !Objects.equals(e.getSnils(), tmp.get(0).getSnils()) &&
                                e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                !Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                !e.getLastName().equals(tmp.get(0).getLastName()) &&
                                e.getFirstName().equals(tmp.get(0).getFirstName())
                        ).map(e -> "" + e.getId()).collect(Collectors.toList());

                stage7Ids.add("" + tmp.get(0).getId());

                if(stage7Ids.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_EXCEPT_SNILS_MIDDLE_LAST)
                            .add(enp + "|" + String.join(",", stage7Ids));
                }
                //#endregion

                //region stage 3 (full doubles)
                List<String> doubleIds = tmp.stream()
                        .filter(e -> Objects.equals(e.getSnils(), tmp.get(0).getSnils()) &&
                                e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                e.getFirstName().equals(tmp.get(0).getFirstName()) &&
                                e.getLastName().equals(tmp.get(0).getLastName()))
                        .map(e ->"" + e.getId()).collect(Collectors.toList());

                if(doubleIds.size() > 1) {
                    stats.getDoubles()
                            .get(Constants.DOUBLES_FULL).add(enp + "|" + String.join(",", doubleIds));
                }
                //endregion
            }
        }

        return stats;
    }
}

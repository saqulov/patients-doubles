package doubles.api.service;

import doubles.api.model.PersonDto;
import doubles.api.repo.Repo;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@ApplicationScoped
public class RemoveService {

    @Inject private Repo repo;

    public int remove(String filter, String type) throws Exception {
        switch (type) {
            case "enp":
                return removeByENP(filter);
            case "snils":
                return removeBySNILS(filter);
            default: return -1;
        }
    }

    private int removeByENP(String filter) throws Exception {
        List<Long> removable = new ArrayList<>();

        List<PersonDto> persons = repo.searchByENP(10000);

        Set<String> enps = persons.stream().map(PersonDto::getEnp).collect(Collectors.toSet());

        switch (filter) {
            case "full":

                for (String enp : enps) {
                    List<PersonDto> tmp = persons
                            .stream()
                            .filter(e -> e.getEnp().equals(enp))
                            .collect(Collectors.toList());

                    if (!tmp.isEmpty() && tmp.size() > 1) {
                        List<Long> ids = tmp.stream()
                                .filter(e -> Objects.equals(e.getSnils(), tmp.get(0).getSnils()) &&
                                        e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                        Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                        e.getFirstName().equals(tmp.get(0).getFirstName()) &&
                                        e.getLastName().equals(tmp.get(0).getLastName()))
                                .map(PersonDto::getId).collect(Collectors.toList());

                        if (ids.size() > 1) {
                            removable.addAll(ids.subList(1, ids.size()));
                        }
                    }

                }
                break;
            case "middleAdded":  //working version
                for (String enp : enps) {
                    List<PersonDto> tmp = persons
                            .stream()
                            .filter(e -> e.getEnp().equals(enp))
                            .sorted(Comparator.comparing(PersonDto::getId))
                            .collect(Collectors.toList());

                    if (!tmp.isEmpty() && tmp.size() > 1) {
                        boolean search;

                        Optional<PersonDto> withNonNullMiddle = tmp.stream().filter(e -> e.getMiddleName() != null).findFirst();

                        if (withNonNullMiddle.isPresent()) {
                            PersonDto comparable = withNonNullMiddle.get();

                            search = tmp.stream()
                                    .anyMatch(e -> Objects.equals(e.getSnils(), comparable.getSnils()) &&
                                            e.getBirthDate().equals(comparable.getBirthDate()) &&
                                            (e.getMiddleName() == null && !Objects.equals(comparable.getMiddleName(), e.getMiddleName())) &&
                                            e.getFirstName().equals(comparable.getFirstName()) &&
                                            e.getLastName().equals(comparable.getLastName()));
                            if (search) {
                                Optional<PersonDto> withNullMiddle = tmp.stream().filter(e -> e.getMiddleName() == null).findFirst();

                                withNullMiddle.ifPresent(p -> removable.add(p.getId()));
                            }
                        }

                    }

                }
                break;
            case "middleChanged":
                for (String enp : enps) {
                    List<PersonDto> tmp = persons
                            .stream()
                            .filter(e -> e.getEnp().equals(enp))
                            .sorted(Comparator.comparing(PersonDto::getId))
                            .collect(Collectors.toList());

                    if (!tmp.isEmpty() && tmp.size() > 1 &&
                            tmp.stream().allMatch(e -> e.getMiddleName() != null)) {
                        PersonDto comparable = tmp.get(0);

                        boolean search = tmp.stream()
                                .anyMatch(e -> Objects.equals(e.getSnils(), comparable.getSnils()) &&
                                        e.getBirthDate().equals(comparable.getBirthDate()) &&
                                        !comparable.getMiddleName().equals(e.getMiddleName()) &&
                                        e.getFirstName().equals(comparable.getFirstName()) &&
                                        e.getLastName().equals(comparable.getLastName()));
                        if (search) {
                            Optional<PersonDto> withChangedMiddle = tmp.stream().filter(e -> !e.getMiddleName().equals(comparable.getMiddleName())).findFirst();

                            withChangedMiddle.ifPresent(p -> removable.add(p.getId()));
                        }
                    }
                }
                break;
        }

        return repo.remove(removable);
    }

    private int removeBySNILS(String filter) throws Exception {
        List<Long> removable = new ArrayList<>();

        List<PersonDto> persons = repo.searchBySNILS(10000);

        Set<String> snilsSet = persons.stream().map(PersonDto::getSnils).collect(Collectors.toSet());

        switch (filter) {
            case "full":

                for (String snils : snilsSet) {
                    List<PersonDto> tmp = persons
                            .stream()
                            .filter(e -> e.getSnils().equals(snils))
                            .collect(Collectors.toList());

                    if (!tmp.isEmpty() && tmp.size() > 1) {
                        List<Long> ids = tmp.stream()
                                .filter(e -> Objects.equals(e.getEnp(), tmp.get(0).getEnp()) &&
                                        e.getBirthDate().equals(tmp.get(0).getBirthDate()) &&
                                        Objects.equals(e.getMiddleName(), tmp.get(0).getMiddleName()) &&
                                        e.getFirstName().equals(tmp.get(0).getFirstName()) &&
                                        e.getLastName().equals(tmp.get(0).getLastName()))
                                .map(PersonDto::getId).collect(Collectors.toList());

                        if (ids.size() > 1) {
                            removable.addAll(ids.subList(1, ids.size()));
                        }
                    }

                }
                break;
            case "middleAdded":  //working version
                for (String snils : snilsSet) {
                    List<PersonDto> tmp = persons
                            .stream()
                            .filter(e -> e.getSnils().equals(snils))
                            .sorted(Comparator.comparing(PersonDto::getId))
                            .collect(Collectors.toList());

                    if (!tmp.isEmpty() && tmp.size() > 1) {
                        boolean search;

                        Optional<PersonDto> withNonNullMiddle = tmp.stream().filter(e -> e.getMiddleName() != null).findFirst();

                        if (withNonNullMiddle.isPresent()) {
                            PersonDto comparable = withNonNullMiddle.get();

                            search = tmp.stream()
                                    .anyMatch(e -> Objects.equals(e.getEnp(), comparable.getEnp()) &&
                                            e.getBirthDate().equals(comparable.getBirthDate()) &&
                                            (e.getMiddleName() == null && !Objects.equals(comparable.getMiddleName(), e.getMiddleName())) &&
                                            e.getFirstName().equals(comparable.getFirstName()) &&
                                            e.getLastName().equals(comparable.getLastName()));
                            if (search) {
                                Optional<PersonDto> withNullMiddle = tmp.stream().filter(e -> e.getMiddleName() == null).findFirst();

                                withNullMiddle.ifPresent(p -> removable.add(p.getId()));

                                //or may me?
//                                if(withNullMiddle.isPresent()) {
//                                    removable.add(comparable.getId());
//                                }
                            }
                        }

                    }

                }
                break;
            case "middleChanged":
                for (String snils : snilsSet) {
                    List<PersonDto> tmp = persons
                            .stream()
                            .filter(e -> e.getSnils().equals(snils))
                            .sorted(Comparator.comparing(PersonDto::getId))
                            .collect(Collectors.toList());

                    if (!tmp.isEmpty() && tmp.size() > 1 &&
                            tmp.stream().allMatch(e -> e.getMiddleName() != null)) {
                        PersonDto comparable = tmp.get(0);

                        boolean search = tmp.stream()
                                .anyMatch(e -> Objects.equals(e.getEnp(), comparable.getEnp()) &&
                                        e.getBirthDate().equals(comparable.getBirthDate()) &&
                                        !comparable.getMiddleName().equals(e.getMiddleName()) &&
                                        e.getFirstName().equals(comparable.getFirstName()) &&
                                        e.getLastName().equals(comparable.getLastName()));
                        if (search) {
                            Optional<PersonDto> withChangedMiddle = tmp.stream().filter(e -> !e.getMiddleName().equals(comparable.getMiddleName())).findFirst();

                            withChangedMiddle.ifPresent(p -> removable.add(p.getId()));
                        }
                    }
                }
                break;
        }

        return repo.remove(removable);
    }
}

package doubles.api.model;

import com.google.gson.annotations.SerializedName;
import doubles.api.Constants;

import java.util.*;

public class StatisticDto {

    @SerializedName("doubles")
    private Map<String, List<String>> doubles;
    @SerializedName("counters")
    private Map<String, Integer> counters;
    @SerializedName("doubles_by_enp_count")
    private int doublesByENP;
    @SerializedName("doubles_by_snils_count")
    private int doublesBySNILS;

    public void setDoublesByENP(int doublesByENP) {
        this.doublesByENP = doublesByENP;
    }

    public void setDoublesBySNILS(int doublesBySNILS) {
        this.doublesBySNILS = doublesBySNILS;
    }

    public Map<String, List<String>> getDoubles() {
        return doubles;
    }

    public void setCounts() {
        counters = new HashMap<>();
        counters.put(Constants.DOUBLES_FULL + "_count", doubles.get(Constants.DOUBLES_FULL).size());
        counters.put(Constants.DOUBLES_EXCEPT_LAST + "_count", doubles.get(Constants.DOUBLES_EXCEPT_LAST).size());
        counters.put(Constants.DOUBLES_EXCEPT_MIDDLE + "_count", doubles.get(Constants.DOUBLES_EXCEPT_MIDDLE).size());
        counters.put(Constants.DOUBLES_EXCEPT_MIDDLE_AND_LAST + "_count", doubles.get(Constants.DOUBLES_EXCEPT_MIDDLE_AND_LAST).size());
        counters.put(Constants.DOUBLES_EXCEPT_SNILS + "_count", doubles.get(Constants.DOUBLES_EXCEPT_SNILS).size());
        counters.put(Constants.DOUBLES_EXCEPT_SNILS_LAST + "_count", doubles.get(Constants.DOUBLES_EXCEPT_SNILS_LAST).size());
        counters.put(Constants.DOUBLES_EXCEPT_SNILS_MIDDLE + "_count", doubles.get(Constants.DOUBLES_EXCEPT_SNILS_MIDDLE).size());
        counters.put(Constants.DOUBLES_EXCEPT_SNILS_MIDDLE_LAST + "_count", doubles.get(Constants.DOUBLES_EXCEPT_SNILS_MIDDLE_LAST).size());
        counters.put(Constants.DOUBLES_EXCEPT_ENP + "_count", doubles.get(Constants.DOUBLES_EXCEPT_ENP).size());
        counters.put(Constants.DOUBLES_EXCEPT_ENP_LAST + "_count", doubles.get(Constants.DOUBLES_EXCEPT_ENP_LAST).size());
        counters.put(Constants.DOUBLES_EXCEPT_ENP_MIDDLE + "_count", doubles.get(Constants.DOUBLES_EXCEPT_ENP_MIDDLE).size());
        counters.put(Constants.DOUBLES_EXCEPT_ENP_MIDDLE_LAST + "_count", doubles.get(Constants.DOUBLES_EXCEPT_ENP_MIDDLE_LAST).size());

    }

    public StatisticDto() {
        doubles = new HashMap<>();
        doubles.put(Constants.DOUBLES_FULL, new ArrayList<String>());
        doubles.put(Constants.DOUBLES_EXCEPT_LAST, new ArrayList<String>());
        doubles.put(Constants.DOUBLES_EXCEPT_MIDDLE, new ArrayList<String>());
        doubles.put(Constants.DOUBLES_EXCEPT_MIDDLE_AND_LAST, new ArrayList<String>());
        doubles.put(Constants.DOUBLES_EXCEPT_SNILS, new ArrayList<>());
        doubles.put(Constants.DOUBLES_EXCEPT_SNILS_LAST, new ArrayList<>());
        doubles.put(Constants.DOUBLES_EXCEPT_SNILS_MIDDLE, new ArrayList<>());
        doubles.put(Constants.DOUBLES_EXCEPT_SNILS_MIDDLE_LAST, new ArrayList<>());
        doubles.put(Constants.DOUBLES_EXCEPT_ENP, new ArrayList<>());
        doubles.put(Constants.DOUBLES_EXCEPT_ENP_LAST, new ArrayList<>());
        doubles.put(Constants.DOUBLES_EXCEPT_ENP_MIDDLE, new ArrayList<>());
        doubles.put(Constants.DOUBLES_EXCEPT_ENP_MIDDLE_LAST, new ArrayList<>());
    }
}

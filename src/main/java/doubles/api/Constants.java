package doubles.api;

public final class Constants {
    //DIFF TYPES
    public static String DOUBLES_FULL = "doubles_by_all_filters";
    public static String DOUBLES_EXCEPT_LAST = "doubles_except_last";
    public static String DOUBLES_EXCEPT_MIDDLE = "doubles_except_middle";
    public static String DOUBLES_EXCEPT_MIDDLE_AND_LAST = "doubles_except_middle_and_last";
    public static String DOUBLES_EXCEPT_SNILS = "doubles_except_snils";
    public static String DOUBLES_EXCEPT_SNILS_MIDDLE_LAST = "doubles_except_snils_middle_last";
    public static String DOUBLES_EXCEPT_SNILS_LAST = "doubles_except_snils_last";
    public static String DOUBLES_EXCEPT_SNILS_MIDDLE = "doubles_except_snils_middle";
    public static String DOUBLES_EXCEPT_ENP = "doubles_except_enp";
    public static String DOUBLES_EXCEPT_ENP_MIDDLE_LAST = "doubles_except_enp_middle_last";
    public static String DOUBLES_EXCEPT_ENP_LAST = "doubles_except_enp_last";
    public static String DOUBLES_EXCEPT_ENP_MIDDLE = "doubles_except_enp_middle";


    // OPERATIONS
    public static String DELETE_FROM_PC = "delete from core.person_characteristics where person = ";
    public static String DELETE_FROM_ADDR = "delete from core.address where person = ";
    public static String DELETE_FROM_ATTACH = "delete from core.attachment where person = ";
    public static String DELETE_FROM_DOC = "delete from core.document where person = ";
    public static String DELETE_FROM_INSURANCE = "delete from core.insurance where person = ";
    public static String DELETE_FROM_PERSON = "delete from core.person where id = ";

}

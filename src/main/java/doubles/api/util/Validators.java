package doubles.api.util;

public class Validators {

    public static boolean equals(String left, String right) {
        boolean result = false;

        if(left == null && right == null)
            result = true;
        else if(left != null && left.equals(right))
            result = true;

        return result;
    }
}

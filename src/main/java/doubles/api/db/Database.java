package doubles.api.db;

import doubles.AppException;
import doubles.AppProperties;
import doubles.Configuration;
import doubles.api.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vibur.dbcp.ViburDBCPDataSource;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;

@ApplicationScoped
public class Database {

    private static final Logger LOG = LoggerFactory.getLogger(Database.class);

    private ViburDBCPDataSource ds;

    private final ExecutorService executorService = Executors.newFixedThreadPool(
            Configuration.getDatabaseThreadCount());

    @Inject
    private AppProperties appProperties;

    public Database() {
    }

    @PostConstruct
    public void init() {
        ds = initDS();
    }

    private ViburDBCPDataSource initDS() {
        appProperties.initProperties(true);

        ViburDBCPDataSource ds = new ViburDBCPDataSource();
        ds.setJdbcUrl(appProperties.getConnection());
        ds.setUsername(appProperties.getLogin());
        ds.setPassword(appProperties.getPassword());
        ds.start();
        return ds;
    }

    public <T> T execute(String sql, List<Object> params, Function<ResultSet, T> mapper) {
        try(Connection c = ds.getConnection()) {
            return operation(c, sql, params, mapper);
        } catch (SQLException e) {
            LOG.error("ошибка при выполнении получении запроса к db", e);
            throw new RuntimeException("ошибка при выполнении получении запроса к db", e);
        }
    }

    public long executeInsert(String sql, List<Object> params) {
        try (Connection c = ds.getConnection()) {
            return prepareInsert(c, sql, params);
        } catch (SQLException e) {
            LOG.error("Ошибка при выполнении запроса к бд", e);
            throw new AppException("Внутренняя ошибка сервера");
        }
    }

    public long executeUpdate(String sql, List<Object> params) {
        try (Connection c = ds.getConnection()) {
            return prepareInsert(c, sql, params);
        } catch (SQLException e) {
            LOG.error("Ошибка при выполнении запроса к бд", e);
            throw new AppException("Внутренняя ошибка сервера");
        }
    }

    private long prepareInsert(Connection c, String sql, List<Object> params) throws SQLException {
        try (PreparedStatement ps = c.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
            prepareStatement(ps, params);
            return fetchInsert(ps);
        }
    }

    private long prepareUpdate(Connection c, String sql, List<Object> params) throws SQLException {
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            prepareStatement(ps, params);
            return fetchUpdate(ps);
        }
    }

    private long fetchInsert(PreparedStatement ps) throws SQLException {
        ps.executeUpdate();

        try (ResultSet rs = ps.getGeneratedKeys()) {
            rs.next();
            return rs.getLong(1);
        }
    }

    private int fetchUpdate(PreparedStatement ps) throws SQLException {
        return ps.executeUpdate();
    }

    public <T> T operation(Connection c, String sql, List<Object> params, Function<ResultSet, T> mapper) {
        try(PreparedStatement ps = c.prepareStatement(sql)) {
            prepareStatement(ps, params);
            return fetchData(ps, mapper);
        } catch (SQLException e) {
            LOG.error("ошибка при формировании");
            throw new RuntimeException("ошибка при формировании", e);
        }
    }

    private void prepareStatement(PreparedStatement ps, List<Object> params) throws SQLException {
        for (int i = 0; i < params.size(); i++) {
            ps.setObject(i + 1, params.get(i));
        }
    }

    private <T> T fetchData(PreparedStatement ps, Function<ResultSet, T> mapper) {
        try(ResultSet rs = ps.executeQuery()) {
            return mapper.apply(rs);
        } catch (SQLException e) {
            LOG.error("ошибка при получении данных из db");
            throw new RuntimeException("ошибка при получении данных из db", e);
        }
    }

    public int executeBatchTransaction(List<Long> ids)
            throws SQLException {
        List<Integer> affected = new ArrayList<>();

        Savepoint save = null;
        try(Connection c = ds.getConnection()) {
            c.setAutoCommit(false);
            try(Statement statement = c.createStatement()) {
                for(Long id : ids) {
                    save = c.setSavepoint();

                    statement.addBatch(Constants.DELETE_FROM_PC + id);
                    statement.addBatch(Constants.DELETE_FROM_ATTACH + id);
                    statement.addBatch(Constants.DELETE_FROM_ADDR + id);
                    statement.addBatch(Constants.DELETE_FROM_DOC + id);
                    statement.addBatch(Constants.DELETE_FROM_INSURANCE + id);
                    statement.addBatch(Constants.DELETE_FROM_PERSON + id);

                    int[] currentAffected = statement.executeBatch();
                    affected.addAll(Arrays
                                    .stream(currentAffected)
                                    .boxed()
                                    .collect(Collectors.toList()));
                }
                c.commit();
            } catch (Exception e) {
                if(save != null)
                    c.rollback(save);
                LOG.error(e.getMessage());
                throw new RuntimeException("ошибка при удалении дублей из db", e);
            }
        }
        return affected.stream().reduce(0, Integer::sum);
    }

    @PreDestroy
    public void clean() {
        executorService.shutdown();
        ds.terminate();
    }
}

package doubles.api.repo;

import doubles.api.db.Database;
import doubles.api.model.PersonDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class Repo {

    private static final Logger LOG = LoggerFactory.getLogger(Repo.class);

    @Inject private Database database;

    public Repo() {
    }

    public List<PersonDto> searchByENP(int limit) {
        List<Object> params = new ArrayList<>();
        params.add(limit);

        return database.execute("" +
                "with persons as (select\n" +
                "enp, count(enp)\n" +
                "from core.person\n" +
                "group by enp\n" +
                "having count(enp) > 1)\n" +
                "select *\n" +
                "from core.person\n" +
                "where enp in (select enp from persons)\n" +
                "order by enp\n limit ?", params, Repo::map);
    }
    public List<PersonDto> searchBySNILS(int limit) {
        List<Object> params = new ArrayList<>();
        params.add(limit);

        return database.execute("" +
                "with persons as (select\n" +
                "snils, count(snils)\n" +
                "from core.person\n" +
                "where snils is not null\n" +
                "group by snils\n" +
                "having count(snils) > 1)\n" +
                "select *\n" +
                "from core.person\n" +
                "where snils in (select snils from persons)\n" +
                "order by snils\n limit ?", params, Repo::map);
    }

    private static List<PersonDto> map(ResultSet rs) {
        List<PersonDto> persons = new ArrayList<>();
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendPattern("yyyy-MM-dd")
                .optionalStart()
                .appendPattern(" HH:mm")
                .optionalEnd()
                .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
                .toFormatter();
        try {
            while(rs.next()) {
                PersonDto person = new PersonDto();

                person.setId(rs.getLong("id"));
                person.setCreateDate(formatter.format(rs.getTimestamp("create_date").toLocalDateTime()));

                if(rs.getObject("update_date") != null)
                    person.setUpdateDate(formatter.format(rs.getTimestamp("update_date").toLocalDateTime()));

                person.setEventId(rs.getLong("event_id"));

                person.setDeleted(rs.getBoolean("deleted"));
                person.setDead(rs.getBoolean("dead"));

                if(rs.getObject("death_date") != null)
                    person.setDeathDate(formatter.format(rs.getDate("death_date").toLocalDate()));

                person.setBirthDate(formatter.format(rs.getDate("birth_date").toLocalDate()));

                if(rs.getObject("enp") != null)
                    person.setEnp(rs.getString("enp"));

                if(rs.getObject("snils") != null)
                    person.setSnils(rs.getString("snils"));

                person.setFirstName(rs.getString("first_name"));
                person.setLastName(rs.getString("last_name"));

                if(rs.getObject("hash") != null)
                    person.setMd5(rs.getString("hash"));

                if(rs.getObject("middle_name") != null)
                    person.setMiddleName(rs.getString("middle_name"));

                if(rs.getObject("gender") != null)
                    person.setGender(rs.getString("gender"));

                person.setBase_info();
                persons.add(person);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new RuntimeException(e);
        }
        return persons;
    }

    public PersonDto search(long id) {
        List<Object> params = new ArrayList<>();
        params.add(id);
        List<PersonDto> persons = database.execute("" +
                "select * from core.person where id = ?", params, Repo::map);

        if(!persons.isEmpty())
            return  persons.get(0);
        return null;
    }

    public int remove(List<Long> ids) throws SQLException {
       return database.executeBatchTransaction(ids);
    }
}

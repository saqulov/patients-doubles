package doubles.api.rsrc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import doubles.api.model.PersonDto;
import doubles.api.repo.Repo;
import doubles.api.service.RemoveService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("search")
@ApplicationScoped
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
public class Search {

    @Inject private Repo repo;
    @Inject private RemoveService service;

    @GET
    @Path("/show")
    @Produces({MediaType.APPLICATION_JSON})
    public Response show(
            @DefaultValue("1000") @QueryParam("limit") Integer limit,
            @DefaultValue("enp") @QueryParam("filter") String filter
            ) {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setPrettyPrinting()
                .create();

        List<PersonDto> personList = null;

        if(filter.equals("enp"))
            personList = repo.searchByENP(limit);
        else if(filter.equals("snils"))
            personList = repo.searchBySNILS(limit);

        return Response
                .status(Response.Status.OK)
                .entity(gson.toJson(personList))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    @DELETE
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response delete(
            @DefaultValue("full") @QueryParam("filter") String filter,
            @DefaultValue("enp") @QueryParam("type") String type) {
        int affected = 0;
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setPrettyPrinting()
                .create();
        try {
            affected = service.remove(filter, type);
        } catch (Exception e) {
            return Response
                    .status(Response.Status.CONFLICT)
                    .type(MediaType.APPLICATION_JSON)
                    .entity(gson.toJson(e))
                    .build();
        }

        return Response
                .status(Response.Status.OK)
                .type(MediaType.TEXT_PLAIN)
                .entity(affected)
                .build();
    }
}

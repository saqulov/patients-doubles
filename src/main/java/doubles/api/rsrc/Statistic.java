package doubles.api.rsrc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import doubles.api.service.StatisticService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("stats")
@ApplicationScoped
@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
public class Statistic {

    @Inject private StatisticService service;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@DefaultValue ("enp") @QueryParam("filter") String filter) {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setPrettyPrinting()
                .create();

        String stats = gson.toJson(service.calculate(filter));
        return Response
                .status(Response.Status.OK)
                .entity(stats)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    @GET
    @Path("/compare")
    @Produces(MediaType.TEXT_PLAIN)
    public Response compare(@QueryParam("first") long first,
                            @QueryParam("second") long second) {

        String stats = service.compare(first, second);

        if(stats.trim().equals(""))
            return  Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity("no records found")
                    .type(MediaType.TEXT_PLAIN)
                    .build();

            return Response
                    .status(Response.Status.OK)
                    .entity(stats)
                    .type(MediaType.APPLICATION_JSON)
                    .build();
    }
}

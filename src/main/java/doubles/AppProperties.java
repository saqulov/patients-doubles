package doubles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@ApplicationScoped
public class AppProperties {
    private final Logger LOG = LoggerFactory.getLogger(AppProperties.class);
    private final String CONNECTION = "jdbc:postgresql://q4-demo2.medsoft.su:6432/mpi?prepareThreshold=0&characterEncoding=utf8";
    private final String LOGIN = "mpi";
    private final String PASSWORD = "mpi";

    private String pwd;
    private String connection;
    private String login;

    public AppProperties() {

    }

    //#region getters

    public String getConnection() {
        return connection;
    }
    public String getPassword() {
        return pwd;
    }
    public String getLogin(){
        return  login;
    }

    //#endregion

    @PostConstruct
    public void init() {
        initProperties(false);
    }

    @PreDestroy
    void clean() {

    }

    public void initProperties(boolean isTest)  {

        if (isTest) {
            connection = CONNECTION;
            pwd = PASSWORD;
            login = LOGIN;

            return;
        }

        boolean result = false;
        try {
            result = getAppProperties();

        } catch (Exception e) {
            LOG.debug(e.getMessage());
        }
        finally {
            if(!result) getSysProperties();
        }
    }
    private boolean getAppProperties() throws IOException {
        InputStream inputStream = null;

        try {
            Properties props = new Properties();
            String propFilename = "application.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFilename);

            if (inputStream != null) {
                props.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFilename + "' not found in the classpath");
            }

            connection = props.getProperty("rir.mpi.database.connection");
            login = props.getProperty("rir.mpi.database.user");
            pwd = props.getProperty("rir.mpi.database.password");

            return checkNulls("Application");

        } catch (Exception e){
            LOG.debug(e.getMessage());

            return false;
        } finally {
            inputStream.close();
        }
    }

    private boolean getSysProperties(){
        connection = System.getProperty("rir.mpi.database.connection");
        login = System.getProperty("rir.mpi.database.user");
        pwd = System.getProperty("rir.mpi.database.password");

        return checkNulls("wildfly");
    }

    private boolean checkNulls(String source) {
        boolean isNull = false;

        if(connection == null) {
            LOG.debug("Source: "  + source + ";Database ConnectionString is null.");
            isNull = true;
        }
        if(pwd == null) {
            LOG.debug("Source: "  + source + ";Database credentials: password is null.");
            isNull = true;
        }
        if(login == null) {
            LOG.debug("Source: "  + source + ";Database credentials: login is null.");
            isNull = true;
        }
        return isNull;
    }
}


package doubles;

public class Configuration {

    public static int getDatabaseThreadCount() {

        String threadCountProperty = System.getProperty("rir.mpi.database.thread_count");

        if (threadCountProperty == null) {
            return 10;
        } else {
            return Integer.getInteger(threadCountProperty);
        }

    }
}

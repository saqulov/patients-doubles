package doubles;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@ApplicationPath("/api")
@ApplicationScoped
public class DoublesService extends Application {

    @PostConstruct
    public void init() {
    }
}
